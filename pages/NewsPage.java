package pagefactory.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static org.openqa.selenium.By.xpath;

public class NewsPage extends BasePage {

    @FindBy(xpath = "//h2[@class='gel-double-pica-bold']")
    private WebElement searchInput;

    public NewsPage(WebDriver driver) {
        super(driver);
    }

    public void search(final String toSearch) {
        driver.findElement((By) searchInput).sendKeys(toSearch);
    }


}
